package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;

public class App {
	public static void main(String[] args) {
		CriadorEquipes criarEquipe = new CriadorEquipes();
		List<Equipe> equipe = new ArrayList<>(); 
		equipe = criarEquipe.construir();
		Impressora.imprimir(equipe);	
	}
}


