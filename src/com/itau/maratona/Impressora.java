package com.itau.maratona;

import java.util.List;

public class Impressora {

	public static void imprimir(List<Equipe> equipes) {
		for(Equipe equipe: equipes) {
			System.out.println(equipe);
		}
	}

}
