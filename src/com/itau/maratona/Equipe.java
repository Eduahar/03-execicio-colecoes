package com.itau.maratona;

import java.util.List;

public class Equipe {
	public int id;
	public List<Aluno> aluno;
	
	public String toString() {
		String texto = new String();
		texto = "Equipe Id : " +this.id;
		texto += "\n";
		
		for (Aluno alunos : aluno) {
			texto += "nome: " +alunos.nome +" ";
			texto += "cpf : " +alunos.cpf +" ";
			texto += "email : " +alunos.email +" ";	
			texto += "\n";
		}
		return texto;
	}
}
